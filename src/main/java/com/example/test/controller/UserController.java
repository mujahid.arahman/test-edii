package com.example.test.controller;

import com.example.test.model.User;
import com.example.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/getDataUser")
    public Object getDataUser(@RequestParam String userId){
        return userService.getDataUser(userId);
    }

    @PostMapping("/setDataUser")
    public Object setDataUser(@RequestBody User user){
        return userService.setDataUser(user);
    }

    @DeleteMapping("/delDataUser")
    public void delDataUser(@RequestParam Integer userId){
        userService.delDataUser(userId);
    }

}

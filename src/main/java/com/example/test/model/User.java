package com.example.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tbl_user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "userid", nullable = false)
    private Integer userId;

    @Column(name= "namalengkap")
    private String namaLengkap;

    @Column(name= "username")
    private String username;

    @Column(name= "password")
    private String password;

    @Column(name= "status")
    private String status;
}

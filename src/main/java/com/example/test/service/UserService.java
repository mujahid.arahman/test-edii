package com.example.test.service;

import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Object getDataUser(String userId){
        if (userId.equalsIgnoreCase("all")){
            return userRepository.findAll();
        }
        return userRepository.findById(Integer.valueOf(userId));
    }

    public Object setDataUser(User user){
        return userRepository.save(user);
    }

    public void delDataUser(Integer userId){
        userRepository.deleteById(userId);
    }
}
